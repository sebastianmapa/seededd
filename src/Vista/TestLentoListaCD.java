/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaCD;

/**
 *
 * @author DOCENTE
 */
public class TestLentoListaCD {

    public static void main(String[] args) {
        ListaCD<Integer> l = new ListaCD();
        int n = 150000;
        double ini = System.currentTimeMillis();
        while (n-- > 0) {
            l.insertarFinal(n);
        }
        double fin = (System.currentTimeMillis() - ini) / 1000;
        System.out.println("TIempo:" + fin + "seg");

        long total = 0;
        ini = System.currentTimeMillis();
        for (int i = 0; i < l.getSize(); i++) {
            total += l.get(i);

        }
        fin = (System.currentTimeMillis() - ini) / 1000;
        System.out.println("TIempo:" + fin + " seg, total:"+total);

        ini = System.currentTimeMillis();
        total=0;
        for (int dato : l) {
            total += dato;
            //System.out.println(dato);
        }
        fin = (System.currentTimeMillis() - ini) / 1000;
        System.out.println("TIempo:" + fin + " seg, total:"+total);

    }

}
