/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Util.seed.ListaS;
import java.util.Scanner;

/**
 *
 * @author Docente
 */
public class TestOrdenado {
    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        ListaS<Integer> l=new ListaS<Integer>();
        while(true)
        {
            System.out.println("Digite un nùmero o -1 para salir");
            int numero=in.nextInt();
            if(numero==-1)
                break;
            l.insertarOrdenado(numero);
        }
        System.out.println(l.toString());
    }
   
}
